package com.bot.chart;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.LegendPosition;
import org.apache.poi.xddf.usermodel.chart.XDDFChartData;
import org.apache.poi.xddf.usermodel.chart.XDDFChartLegend;
import org.apache.poi.xddf.usermodel.chart.XDDFDataSource;
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory;
import org.apache.poi.xddf.usermodel.chart.XDDFNumericalDataSource;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 * https://roytuts.com/how-to-generate-pie-chart-in-excel-using-apache-poi/
 */
public class PieChartTest {
    public static void main(String[] args) throws IOException {
        pieChart();
    }

    public static void pieChart() throws FileNotFoundException, IOException {
        try (XSSFWorkbook wb = new XSSFWorkbook()) {

            XSSFSheet sheet = wb.createSheet("sheet1");

            XSSFDrawing drawing = sheet.createDrawingPatriarch();
            XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 4, 9, 20);

            XSSFChart chart = drawing.createChart(anchor);
            chart.setTitleText("用户类型图");
            chart.setTitleOverlay(false);

            XDDFChartLegend legend = chart.getOrAddLegend();
            legend.setPosition(LegendPosition.TOP_RIGHT);

            XDDFDataSource<String> categories = XDDFDataSourcesFactory.fromArray(new String[]{"居民用户（ E 类）", "三相一般工商业用户（ C 类）", "单相一般工商业用户（ D 类）"});

            XDDFNumericalDataSource<Integer> values = XDDFDataSourcesFactory.fromArray(new Integer[]{10, 5, 3});

            XDDFChartData data = chart.createData(ChartTypes.PIE3D, null, null);// chart.createData(ChartTypes.PIE,
            // null, null);
            data.setVaryColors(true);
            data.addSeries(categories, values);
            chart.plot(data);

            // Write output to an excel file
            try (FileOutputStream fileOut = new FileOutputStream("FuYang.xlsx")) {
                wb.write(fileOut);
            }
        }
    }
}
